import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Caja from 'App/Models/Caja'
import Log from 'App/Models/Log'
import Product from 'App/Models/Product'
import CajaFuerte from 'App/Models/Cajafuerte'

import { rules, schema } from '@ioc:Adonis/Core/Validator'


export default class CajasController {
    async venta({ auth, request, response } : HttpContextContract){
        
        const sellValidation = await schema.create({
            products : schema.array().members(schema.object().members({
                id : schema.number(),
                cantidad: schema.number(),
                name: schema.string()
            })),
            cantidad : schema.number()
        })

        
        const validate = await request.validate({
            schema: sellValidation,
        })
        
        let errorType : boolean | string = false
        let errorId = 0

        for( let i = 0; i < validate.products.length; i++){
            
            const product = await Product.findBy('id', validate.products[i].id)
            
            if(product == null || product.existencias < validate.products[i].cantidad){
                errorType = validate.products[i].name
                errorId = validate.products[i].id
                break;
                }

            if(auth.user?.id == product.account_id){
                
                product.existencias= product.existencias - validate.products[i].cantidad
                await product.save()

                const log = new Log()
                log.account_id = auth.user?.id
                log.event = `Se vendieron 
                    ${validate.products[i].cantidad} de ${validate.products[i].name}`
                
                await log.save()
            }
            
        }
       
        if(typeof(errorType) == 'string' ){
            return response.status(404).json({
                message : `El producto ${errorType} ya no existe en el inventario o esta agotado`,
                errorId : errorId
            })
        } 

        const caja = await Caja.findBy('account_id', auth.user?.id)
        
        if(caja !== null && caja.account_id == auth.user?.id){
            caja.importe = caja.importe + validate.cantidad
            await caja.save()
        }

        return response.json({
            status: 'sure',
            data: 'Evento registrado'
        })
    }
    async getCajas({auth, response}: HttpContextContract){
        const caja = await Caja.findBy('account_id', auth.user?.id)
        const cajaFuerte = await CajaFuerte.findBy('account_id', auth.user?.id)

        if (caja == null || cajaFuerte == null) {
            return response.status(404).json({
                error: 'Usuario no encontrado'
            })      
        }

        return response.json({
            status: 'sure',
            data: {
                caja: caja,
                cajaFuerte: cajaFuerte
            }
        })
    }

    async cerrarCajas({auth, response}:HttpContextContract){
        const caja = await Caja.findBy('account_id', auth.user?.id)
        const cajaFuerte = await CajaFuerte.findBy('account_id', auth.user?.id)

        if (caja == null || cajaFuerte == null) {
            return response.status(404).json({
                error: 'Usuario no encontrado'
            })      
        }
        
        let cantidadCaja : number = caja.importe
        
        cajaFuerte.importe = cajaFuerte.importe + cantidadCaja
        await cajaFuerte.save()

        caja.importe = 0
        await caja.save()
        
        const log = await new Log()
        log.account_id = auth.user!.id
        log.event = `Se retiraron ${cantidadCaja}$ a Caja fuerte, se cerró caja`
        await log.save()
        

        return response.json({
            status: 'sure',
            data: {
                caja: caja,
                cajaFuerte: cajaFuerte
            }
        })
      
    }
    async retiro({auth, request, response}:HttpContextContract){
        const cajaFuerte = await CajaFuerte.findBy('account_id', auth.user?.id)
        
        if (cajaFuerte == null) {
            return response.status(404).json({
                status: 'Not found',
                message: 'Este usuario no fue localizado'
            })
        }
        
        const validation = await schema.create({
            cantidad : schema.number([
                rules.required()
            ])
        })
        
        const validate = await request.validate({
            schema: validation,
        })

        if (cajaFuerte.importe < validate.cantidad) {
           return response.status(401).json({
                status: 'not permissions',
                message: 'Lo sentimos usted no dispone de fondos suficientes'
           })       
        }

        cajaFuerte.importe = cajaFuerte.importe - validate.cantidad 
        await cajaFuerte.save()
        
        const log = await new Log()
        log.account_id = auth.user!.id
        log.event = `Se retiraron ${validate.cantidad}$ de caja Fuerte, quedan ${cajaFuerte.importe}$`
        await log.save()

        return response.json({
            status: 'sure',
            message: 'Retiro exitoso',
            cajaFuerte: cajaFuerte
        })
    }
}
