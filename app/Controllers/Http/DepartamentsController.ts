// import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import { rules, schema } from '@ioc:Adonis/Core/Validator'
import Departament from 'App/Models/Departament'
import Log from 'App/Models/Log'

export default class DepartamentsController {
    async registrarDepartamento({ request, auth, response }:HttpContextContract) {
        const validation = schema.create({
            nombre: schema.string({}, [
                rules.required()
            ])
        });
        const messages = {
            "required": "Es necesario llenar el campo del nombre",
        };
        const validate = await request.validate({
            schema: validation,
            messages: messages
        });
        const departamento = new Departament;
        departamento.account_id = auth.user!.id;
        departamento.name = validate.nombre;
        await departamento.save();
        return response.json({
            status: 'sure',
            message: 'registrado',
            data: departamento
        });
    }
    async eliminardeparamento({ params, auth, response }) {
        const deparamento = await Departament.findBy('id', params.id);
        if (deparamento == null) {
            return response.status(404).json({
                status: 'Not found',
                message: 'Este producto ya ha sido eliminado'
            });
        }
        if (deparamento.account_id == auth.user.id) {
            await deparamento.delete();
        }
    }


    async editarDepartamento({auth, request, response, params}: HttpContextContract){
        const departamento = await Departament.findBy('id', params.id);
        
        if (departamento == null) {
            return response.status(404).json({
                status: 'Not found',
                message: 'Este producto ya ha sido eliminado'
            });
        }        

        if (departamento.account_id !== auth.user!.id) {
            return response.status(401).json({
                status: 'wrong',
                message: 'Unauthorized'
            })
        }

        const validation = schema.create({
            nombre: schema.string({}, [
                rules.required()
            ])
        });
        const messages = {
            "required": "Es necesario llenar el campo del nombre",
        };
        const validate = await request.validate({
            schema: validation,
            messages: messages
        });
         
        departamento.account_id = auth.user!.id;
        departamento.name = validate.nombre;
        await departamento.save();
        
        const log = new Log()
        log.account_id = auth.user!.id
        log.event = `Se Editó la departamento ${departamento.name}`
        await log.save()


        return response.json({
            status: 'sure',
            message: 'Registrado',
            data: departamento
        });

    }
}


