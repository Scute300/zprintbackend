import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import { rules, schema } from '@ioc:Adonis/Core/Validator'
import Empleado from "App/Models/Empleado"
import Log from "App/Models/Log"
import Prestamo from 'App/Models/Prestamo'
import { DateTime } from "luxon"
export default class EmpleadosController {
    async addEmpleado({ auth, request, response } : HttpContextContract){
        const vRules = schema.create({
            name: schema.string({},[
                rules.required(),
            ]),
            phone: schema.number([
                rules.required(),
            ]),
            email: schema.string({}, [
                rules.email(),
                rules.required()
            ]),
            sueldo: schema.number([
                rules.required()
            ]),
            gender: schema.string({}, [
                rules.required(),
                rules.maxLength(1)
            ]),
            fecha_contratacion: schema.string({}, [
                rules.required(),
                rules.maxLength(27)
            ]),
            comision: schema.number([
                rules.required(),
                rules.range(1, 100) 
            ]),
            puesto: schema.string.optional({}, [
                rules.maxLength(50)
            ]),
            address: schema.string.optional({}, [
                rules.required(),
                rules.maxLength(200)
            ])
        })

        const messages= {
            "required" : 'Todos los campos con * deben ser llenados'
        }

        const validate = await request.validate({
            schema: vRules,
            messages: messages
        }) 
    
        const empleado = new Empleado()
        empleado.account_id = auth.user!.id
        empleado.name = validate.name 
        empleado.phone = validate.phone.toString()
        empleado.email = validate.email
        empleado.sueldo = validate.sueldo
        empleado.gender = validate.gender
        empleado.fecha_contratacion = DateTime.fromISO(validate.fecha_contratacion).toString()
        empleado.comision = validate.comision 
        empleado.puesto = validate.puesto == undefined ? '' : validate.puesto
        empleado.adress = validate.address == undefined ? '' : validate.address
        await empleado.save()

        return response.json({
            status: 'sure',
            message: 'Empleado agregado'
        })
        
    }

    async getEmpleados({auth, response, params}:HttpContextContract){
        const empleados = await Empleado.query()
        .where('account_id', auth.user!.id)
        .paginate(params.page, 10)

        return response.json({
            status: 'sure',
            data: empleados
        })
    }

    async getDetails({auth, response, params}: HttpContextContract){
        const empleado = await Empleado.findBy('id' , params.id)
        
        if (empleado == null) {
            return response.status(404).json({
                status: 'wrong',
                message: 'not found'
            })
        }

        if (empleado.account_id !== auth.user!.id) {
            return response.status(401).json({
                status: 'wrong',
                message: 'Unauthorized'
            })
        }

        return response.json({
            status: 'sure',
            data: empleado
        })
    }
    
    async editarEmpleado({auth, request, response, params}: HttpContextContract){
        const empleado = await Empleado.findBy('id' , params.id)

        if (empleado == null) {
            return response.status(404).json({
                status: 'wrong',
                message: 'not found'
            })
        }

        if (empleado.account_id !== auth.user!.id) {
            return response.status(401).json({
                status: 'wrong',
                message: 'Unauthorized'
            })
        }

        const vRules = schema.create({
            name: schema.string({},[
                rules.required(),
            ]),
            phone: schema.number([
                rules.required(),
            ]),
            email: schema.string({}, [
                rules.email(),
                rules.required()
            ]),
            sueldo: schema.number([
                rules.required()
            ]),
            gender: schema.string({}, [
                rules.required(),
                rules.maxLength(1)
            ]),
            fecha_contratacion: schema.string({}, [
                rules.required(),
                rules.maxLength(27)
            ]),
            comision: schema.number([
                rules.required(),
                rules.range(1, 100) 
            ]),
            puesto: schema.string.optional({}, [
                rules.maxLength(50)
            ]),
            address: schema.string.optional({}, [
                rules.required(),
                rules.maxLength(200)
            ])
        })

        const messages= {
            "required" : 'Todos los campos con * deben ser llenados'
        }

        const validate = await request.validate({
            schema: vRules,
            messages: messages
        }) 

        empleado.account_id = auth.user!.id
        empleado.name = validate.name 
        empleado.phone = validate.phone.toString()
        empleado.email = validate.email
        empleado.sueldo = validate.sueldo
        empleado.gender = validate.gender
        empleado.fecha_contratacion = DateTime.fromISO(validate.fecha_contratacion).toString()
        empleado.comision = validate.comision 
        empleado.puesto = validate.puesto == undefined ? '' : validate.puesto
        empleado.adress = validate.address == undefined ? '' : validate.address
        await empleado.save()

        const log = new Log()
        log.account_id = auth.user!.id
        log.event = `Se modificaron los datos del empleado ${empleado.name}`
        await log.save()

        return response.json({
            status: 'sure',
            message: 'Empleado editado'
        })
    }

    async eliminarEmpleado({ auth, params, response}: HttpContextContract){
        const empleado = await Empleado.findBy('id' , params.id)
        if (empleado == null) {
            return response.status(404).json({
                status: 'wrong',
                message: 'not found'
            })      
        }

        if (empleado.account_id !== auth.user!.id) {
            return response.status(403).json({
                status: 'wrong',
                message: 'not authenticate'
            })
        }

        await empleado.delete()
        
        const log = new Log()
        log.account_id = auth.user!.id
        log.event = `Se elmiminó al empleado ${empleado.name}`
        await log.save()

        return response.json({
            status: 'sure',
            data: params.id
        })
    }
    
    async nuevoPrestamo({auth, response, request}: HttpContextContract){
        const vRules = schema.create({
            empleado_id: schema.number([
                rules.required(),
                rules.unsigned()
            ]),
            monto: schema.number([
                rules.required()
            ]),
            cuotas: schema.number([
                rules.required()
            ])
        })

        const messages = {
            "required" : "Todos los campos son requeridos"
        }

        const validate = await request.validate({
            schema: vRules,
            messages: messages
        })

        const empleado = await Empleado.findBy('id', validate.empleado_id)
        
        if (empleado == null) {
            return response.status(404).json({
                status: 'wrong',
                message: 'not found'
            })      
        }

        if (empleado.account_id !== auth.user!.id) {
            return response.status(401).json({
                status: 'wrong',
                messages: 'unauthorized'
            })  
        }

        const prestamo = new Prestamo()
        prestamo.empleado_id = validate.empleado_id
        prestamo.monto = validate.monto
        prestamo.cuotas = validate.cuotas
        await prestamo.save()
        
        const log = new Log()
        log.account_id = auth.user!.id
        log.event = `Se le realizó un prestamo a el empleado ${empleado.name}, ${prestamo.monto}$ en ${prestamo.cuotas} cuotas`
        await log.save()

        return response.json({
            status: 'sure',
            data: 'prestamo creado'
        })
    }

    async getAllEmpleados({auth, response}: HttpContextContract){
        const empleados = await Empleado.query()
        .where('account_id', auth.user!.id)

        return response.json({
            status: 'sure',
            data: empleados
        })
    }
}
