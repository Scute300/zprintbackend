import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import  { schema, rules } from '@ioc:Adonis/Core/Validator'
import Empleado from 'App/Models/Empleado'
import Gasto from 'App/Models/Gasto'
import Caja from 'App/Models/Caja'
import CajaFuerte from 'App/Models/Cajafuerte'

export default class GastosController {
    async nuevoGasto({auth, request, response}: HttpContextContract){
        const vRules = schema.create({
            empleado_id: schema.number([
                rules.required()
            ]),
            cantidad: schema.number([
                rules.required()
            ]),
            concepto: schema.string({},[
                rules.required()
            ]),
            descripcion: schema.string({}, [
                rules.required()
            ]),
            caja: schema.boolean([
                rules.required()
            ])
        })


        const messages = {
            "required" : "Todos los campos son requeridos"
        }

        const validate = await request.validate({
            schema: vRules,
            messages: messages
        })

       const empleado = await Empleado.findBy('id' , validate.empleado_id)

        if (empleado == null) {
            return response.status(404).json({
                status: 'wrong',
                message: 'not found'
            })  
        }

        if (empleado.account_id !== auth.user!.id) {
            return response.status(401).json({
                status: 'wrong',
                message: 'unauthorized'
            })
        }
         
        const caja =  !validate.caja ? await CajaFuerte.findBy('account_id', auth.user?.id) : await Caja.findBy('account_id', auth.user?.id)

        if (caja == null) {
            return response.status(404).json({
                status: 'wrong',
                message: 'not found'
            })
        }

        if (caja.importe < validate.cantidad) {
            return response.status(401).json({
                status: 'wrong',
                message: 'No cuenta con fondos suficientes en esta caja'
            })  
        }

        caja.importe = caja.importe - validate.cantidad
        await caja.save()
    

        const gasto = new Gasto()
        gasto.account_id = auth.user!.id
        gasto.empleado_name = empleado.name
        gasto.cantidad = validate.cantidad
        gasto.concepto = validate.concepto
        gasto.descripcion = validate.descripcion
        await gasto.save()

        return response.json({
            status: 'sure',
            data: 'Gasto creado'
        })

    }

}
