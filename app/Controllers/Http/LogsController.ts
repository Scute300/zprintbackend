import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Log from 'App/Models/Log'


export default class LogsController {
    
    async getLogs({ auth, response, params }: HttpContextContract){
        const logs = await Log.query()
        .where('account_id', auth.user!.id)
        .orderBy('createdAt', 'desc')
        .paginate(params.page, 10)

        if (logs == null) {
            return response.status(404).json({
                status: 'not found',
                message: 'User not exist'
            })      
        }

        return response.json({
            status: 'sure',
            data: logs
        })

    }

}
