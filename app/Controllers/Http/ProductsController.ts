import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import { rules, schema } from '@ioc:Adonis/Core/Validator'
import Departament from 'App/Models/Departament'
import Marca from 'App/Models/Marca'
import Product from 'App/Models/Product'
import Application from '@ioc:Adonis/Core/Application'
import randomString from 'randomstring'
import Log from "App/Models/Log"

const SERVER_HOST = process.env.SERVER_HOST;

export default class ProductsController {
    async agregarProducto({ auth, request, response }:HttpContextContract) {
        const vRules = schema.create({
            marca_id: schema.number([
                rules.required(),
                rules.unsigned()
            ]),
            departament_id: schema.number([
                rules.required(),
                rules.unsigned()
            ]),
            product_name: schema.string({}, [
                rules.required(),
            ]),
            codigo_de_barras: schema.number([
                rules.unsigned(),
                rules.required()
            ]),
            precio1: schema.number([
                rules.unsigned(),
                rules.required()
            ]),
            precio2: schema.number([
                rules.unsigned(),
                rules.required()
            ]),
            costo: schema.number([
                rules.unsigned(),
                rules.required()
            ]),
            especificaciones: schema.string({}, [
                rules.required()
            ]),
            existencias: schema.number([
                rules.unsigned(),
                rules.required()
            ]),
            imageProduct: schema.file({
                size: '4mb',
                extnames: ['jpg','png'],  
            })
        });
        const messages = {
            "required": "Es necesario llenar todos los campos",
        };

        
        const validate = await request.validate({
            schema: vRules,
            messages: messages
        });
        const departament = await Departament.query()
            .where('account_id', auth.user!.id)
            .where('id', validate.departament_id)
            .first();
        const marca = await Marca.query()
            .where('account_id', auth.user!.id)
            .where('id', validate.marca_id)
            .first();
        
        const filename : string = randomString.generate({ length : 5, charset: 'hex' })+'_'+auth.user!.name
        let fileFind : any = await Product.findBy('image', SERVER_HOST + filename + '.' + validate.imageProduct.subtype )
        let i : number = 0 
        do {
            i ++
            fileFind = fileFind+i 
        } while (fileFind == null);
        
        await validate.imageProduct.move(Application.publicPath(), {
            name: filename+ '.' + validate.imageProduct.subtype,
        });


        const product = new Product();
        product.product_name = validate.product_name 
        product.marca_id = marca!.id;
        product.departament_id = departament!.id;
        product.account_id = auth.user!.id;
        product.codigo_de_barras = validate.codigo_de_barras;
        product.precio = validate.precio1;
        product.precio_segundo = validate.precio2;
        product.costo = validate.costo;
        product.especificaciones = validate.especificaciones;
        product.existencias = validate.existencias;
        product.image = SERVER_HOST + 'uploads/' + filename + '.' + validate.imageProduct.subtype;
        await product.save();
        return response.json({
            status: 'sure',
            data: 'Nuevo producto agregado'
        });
    }
    
    async getDetails({ auth, response, params }:HttpContextContract){
        const product = await Product.findBy('id' , params.id) 

        if (product == null) {
            return response.status(404).json({
                status: 'wrong',
                message: 'Este producto ya no existe'
            })  
        }

        if (product.account_id !== auth.user!.id) {
            return response.status(401).json({
                status: 'wrong',
                message: 'Unauthorized'
            })
        }

        return response.json({
            status: 'sure',
            data: product
        })
    }

    async editarProducto({ auth, request, response, params }:HttpContextContract) {
        const vRules = schema.create({
            marca_id: schema.number([
                rules.required(),
                rules.unsigned()
            ]),
            departament_id: schema.number([
                rules.required(),
                rules.unsigned()
            ]),
            product_name: schema.string({}, [
                rules.required(),
            ]),
            codigo_de_barras: schema.number([
                rules.unsigned(),
                rules.required()
            ]),
            precio1: schema.number([
                rules.unsigned(),
                rules.required()
            ]),
            precio2: schema.number([
                rules.unsigned(),
                rules.required()
            ]),
            costo: schema.number([
                rules.unsigned(),
                rules.required()
            ]),
            especificaciones: schema.string({}, [
                rules.required()
            ]),
            existencias: schema.number([
                rules.unsigned(),
                rules.required()
            ]),
            imageProduct: schema.file.optional({
                size: '4mb',
                extnames: ['jpg','png'],  
            })
        });
        const messages = {
            "required": "Es necesario llenar todos los campos",
        };

        
        const validate = await request.validate({
            schema: vRules,
            messages: messages
        });
        const departament = await Departament.query()
            .where('account_id', auth.user!.id)
            .where('id', validate.departament_id)
            .first();
        const marca = await Marca.query()
            .where('account_id', auth.user!.id)
            .where('id', validate.marca_id)
            .first();
      
        let filename: string = ''
        if (validate.imageProduct !== undefined) {
            filename = randomString.generate({ length : 5, charset: 'hex' })+'_'+auth.user!.name
            let fileFind : any = await Product.findBy('image', SERVER_HOST + filename + '.' + validate.imageProduct.subtype )
            let i : number = 0 
            do {
                i ++
                fileFind = fileFind+i 
            } while (fileFind == null);
            
            await validate.imageProduct.move(Application.publicPath(), {
                name: filename+ '.' + validate.imageProduct.subtype,
            });   
        }


        const product = await Product.findBy('id', params.id);

        if (product == null) {
           return response.status(404).json({
                status: 'wrong',
                messages: 'El producto que intentas editar no existe'
           })       
        }

        product.product_name = validate.product_name 
        product.marca_id = marca!.id;
        product.departament_id = departament!.id;
        product.account_id = auth.user!.id;
        product.codigo_de_barras = validate.codigo_de_barras;
        product.precio = validate.precio1;
        product.precio_segundo = validate.precio2;
        product.costo = validate.costo;
        product.especificaciones = validate.especificaciones;
        product.existencias = validate.existencias;
        if (filename !== '' && validate.imageProduct !== undefined) { 
            product.image = SERVER_HOST + 'uploads/' + filename + '.' + validate.imageProduct.subtype;   
        }
        await product.save();
        return response.json({
            status: 'sure',
            data: 'Producto editado'
        });
    }

   
    async getProducts({ auth, response, params }) {
        const products = await Product.query()
            .where('account_id', auth.user.id)
            .preload('marca')
            .preload('departament')
            .paginate(params.page, 10)

        return response.json({
            status: 'sure',
            data: products
        });
    }
    async getProductsComplete({ auth, response }) {
        const products = await Product.query()
            .where('account_id', auth.user.id)
            .preload('marca')
            .preload('departament')

        return response.json({
            status: 'sure',
            data: products
        });
    }
    async marcasydepartamentos({ response, auth }:HttpContextContract) {
        const marcas = await Marca.query()
            .where('account_id', auth.user!.id);
        const departaments = await Departament.query()
            .where('account_id', auth.user!.id);
        return response.json({
            status: 'sure',
            marcas: marcas,
            departaments: departaments
        });
    }
    async totales({auth, response}: HttpContextContract ){
        const inventario = await Product.query()
        .where('account_id', auth.user!.id)

        if(inventario == null){
            return response.status(404).json({
                status: 'wrong'
            })
        }
        
        interface total {
            costo: number,
            precio: number,
            utilidad: number,
            unidades: number
        }

        let totalInventario : total = {
            costo: 0,
            precio: 0,
            utilidad: 0,
            unidades: 0
        }

        inventario.map((item) => {
            const totalCosto: number = totalInventario.costo + item.costo * item.existencias
            const totalPrecio: number =  totalInventario.precio = totalInventario.precio + item.precio * item.existencias 
            totalInventario.costo = totalCosto
            totalInventario.precio = totalPrecio
            totalInventario.utilidad = totalPrecio - totalCosto
            totalInventario.unidades = totalInventario.unidades + item.existencias
        })

        return response.json({
            status: 'sure',
            data: totalInventario
        })
    }


    async eliminarProducto({ auth, params, response}: HttpContextContract){
        const product = await Product.findBy('id' , params.id)
        if (product == null) {
            return response.status(404).json({
                status: 'wrong',
                message: 'not found'
            })      
        }

        if (product.account_id !== auth.user!.id) {
            return response.status(403).json({
                status: 'wrong',
                message: 'not authenticate'
            })
        }

        await product.delete()
        
        const log = new Log()
        log.account_id = auth.user!.id
        log.event = `Se elmiminó al producto ${product.product_name}`
        await log.save()

        return response.json({
            status: 'sure',
            data: params.id
        })
    }
}
