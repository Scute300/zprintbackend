import { DateTime } from 'luxon'
import { BaseModel, column, belongsTo, BelongsTo, hasMany, HasMany } from '@ioc:Adonis/Lucid/Orm'
import Account from 'App/Models/Account'
import Prestamo from './Prestamo'

export default class Empleado extends BaseModel {
    @column({ isPrimary: true })
    public id: number

    @column.dateTime({ autoCreate: true })
    public createdAt: DateTime

    @column.dateTime({ autoCreate: true, autoUpdate: true })
    public updatedAt: DateTime

    @column()
    public account_id: number

    @column()
    public name: string

    @column()
    public phone: string

    @column()
    public email: string

    @column()
    public sueldo: number

    @column()
    public gender: string 

    @column()
    public fecha_contratacion: string

    @column()
    public comision: number

    @column()
    public puesto: string

    @column()
    public adress: string

    @belongsTo( () => Account,{
        localKey: 'id',
        foreignKey: 'account_id'
    } )
    public account : BelongsTo<typeof Account>

    @hasMany(() => Prestamo)
    public prestamos: HasMany<typeof Prestamo>
}
