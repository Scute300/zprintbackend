import { DateTime } from 'luxon'
import { BaseModel, column, belongsTo, BelongsTo} from '@ioc:Adonis/Lucid/Orm'
import Account from './Account'
export default class Gasto extends BaseModel {
    @column({ isPrimary: true })
    public id: number

    @column.dateTime({ autoCreate: true })
    public createdAt: DateTime

    @column.dateTime({ autoCreate: true, autoUpdate: true })
    public updatedAt: DateTime

    @column()
    public account_id: number

    @column()
    public empleado_name: string

    @column()
    public cantidad: number 

    @column()
    public concepto: string 

    @column()
    public descripcion: string

    @belongsTo(() => Account , {
        localKey: 'id',
        foreignKey: 'account_id'
    })
    public account: BelongsTo<typeof Account>
}
