import { DateTime } from 'luxon'
import { BaseModel, column, belongsTo, BelongsTo} from '@ioc:Adonis/Lucid/Orm'
import Empleado from './Empleado'

export default class Prestamo extends BaseModel {
    @column({ isPrimary: true })
    public id: number

    @column.dateTime({ autoCreate: true })
    public createdAt: DateTime

    @column.dateTime({ autoCreate: true, autoUpdate: true })
    public updatedAt: DateTime

    @column()
    public empleado_id: number

    @column()
    public monto: number

    @column()
    public cuotas: number

    @belongsTo(() => Empleado,{
        localKey: 'id',
        foreignKey: 'empleado_id'
    })
    public empleado: BelongsTo<typeof Empleado>
}
