import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class Empleados extends BaseSchema {
  protected tableName = 'empleados'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
        table.increments('id')
        table.integer('account_id').notNullable().unsigned().references('id').inTable('accounts')
        table.string('name').notNullable()
        table.string('phone').notNullable()
        table.string('email').notNullable()
        table.float('sueldo').notNullable()
        table.string('gender').notNullable()
        table.timestamp('fecha_contratacion', {useTz: true})
        table.integer('comision')
        table.string('puesto')
        table.string('adress')
        table.timestamp('created_at', { useTz: true })
        table.timestamp('updated_at', { useTz: true })
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
