import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class Prestamos extends BaseSchema {
  protected tableName = 'prestamos'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
        table.increments('id')
        table.integer('empleado_id').notNullable().unsigned().references('id').inTable('empleados')
        table.float('monto')
        table.integer('cuotas').unsigned()
        table.timestamp('created_at', { useTz: true })
        table.timestamp('updated_at', { useTz: true })
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
