import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class Gastos extends BaseSchema {
  protected tableName = 'gastos'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
        table.increments('id')
        table.integer('account_id').unsigned().notNullable().references('id').inTable('accounts').onDelete('CASCADE')
        table.string('empleado_name')
        table.float('cantidad')
        table.string('concepto')
        table.text('descripcion')
        table.timestamp('created_at', { useTz: true })
        table.timestamp('updated_at', { useTz: true })
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
