/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| This file is dedicated for defining HTTP routes. A single file is enough
| for majority of projects, however you can define routes in different
| files and just make sure to import them inside this file. For example
|
| Define routes in following two files
| ├── start/routes/cart.ts
| ├── start/routes/customer.ts
|
| and then import them inside `start/routes/index.ts` as follows
|
| import './cart'
| import './customer'
|
*/

import Route from '@ioc:Adonis/Core/Route'
import Application from '@ioc:Adonis/Core/Application'

Route.post('/register', 'AccountsController.newAccount')
Route.post('/login', 'AccountsController.login')
Route.post('/newPasswordRequest', 'AccountsController.requestNewPassword')
Route.get('/newPassword/:code', 'AccountsController.reestablecerPassword')

Route.get('uploads/:filename', async ({ params, response }) => {
  return response.attachment(
    Application.publicPath(params.filename)
  )
})

Route.group(() => {
    //marcas y departamentos
    Route.post('/newMarca', 'MarcasController.registrarMarca');
    Route.post('/newDepartamento', 'DepartamentsController.registrarDepartamento');
    Route.get('/marcasydepartamentos', 'ProductsController.marcasydepartamentos');
    Route.delete('/eliminarDepartamento/:id', 'DepartamentsController.eliminardeparamento');
    Route.delete('/eliminarMarca/:id', 'MarcasController.eliminarMarca');
    Route.put('/updateMarca/:id', 'MarcasController.editarMarca')
    Route.put('/updateDepartament/:id', 'DepartamentsController.editarDepartamento')

    //productos
    Route.post('/newProduct', 'ProductsController.agregarProducto');
    Route.get('/getProducts/:page', 'ProductsController.getProducts');
    Route.get('/getProducts/', 'ProductsController.getProductsComplete');
    Route.delete('/deleteProduct/:id', 'ProductsController.eliminarProducto')
    Route.get('/detailProduct/:id', 'ProductsController.getDetails')
    Route.put('/updateProduct/:id', 'ProductsController.editarProducto')

    //caja
    Route.post('/newVenta', 'CajasController.venta')
    Route.get('/getCajas', 'CajasController.getCajas')
    Route.post('/cerrarCaja', 'CajasController.cerrarCajas')
    Route.post('/retiro', 'CajasController.retiro')
    Route.get('/totalInventario', 'ProductsController.totales')
    
    // logs
    Route.get('/logs/:page', 'LogsController.getLogs')

    //empleados
    Route.post('/newEmpleado', 'EmpleadosController.addEmpleado')
    Route.get('/getEmpleados/:page', 'EmpleadosController.getEmpleados')
    Route.delete('/eliminarEmpleado/:id', 'EmpleadosController.eliminarEmpleado')
    Route.post('/nuevoPrestamo', 'EmpleadosController.nuevoPrestamo')
    Route.get('/allEmpleados', 'EmpleadosController.getAllEmpleados')
    Route.get('/detailEmpleados/:id', 'EmpleadosController.getDetails')
    Route.put('/updateEmpleado/:id', 'EmpleadosController.editarEmpleado')

    //gastos
    Route.post('/addGasto', 'GastosController.nuevoGasto')

}).prefix('api').middleware('auth');

